package me.imagene.varianteditor.service;

import me.imagene.varianteditor.dto.VariantDTO;
import me.imagene.varianteditor.dto.VariantDTOs;
import me.imagene.varianteditor.exception.VariantException;
import me.imagene.varianteditor.model.Variant;
import me.imagene.varianteditor.repository.VariantRepository;
import me.imagene.varianteditor.utility.DataMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

import static me.imagene.varianteditor.helper.Constants.*;

@Service
@Transactional(rollbackFor = Exception.class)
@SuppressWarnings("rawtypes")
public class VariantServiceImpl {

    private VariantRepository variantRepository;
    private DataMapper<Variant, VariantDTO> dataMapper;

    @Autowired
    public VariantServiceImpl(VariantRepository variantRepository, DataMapper<Variant, VariantDTO> dataMapper) {
        this.variantRepository = variantRepository;
        this.dataMapper = dataMapper;
    }

    public ResponseEntity findOne(Long id){

        if (StringUtils.isEmpty(id.toString())) {
            throw new VariantException(ERROR_VARIANT_ID_REQUIRED);
        }

        Variant variant = variantRepository.findOne(id);
        VariantDTO variantDTO = dataMapper.convert(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }

    public ResponseEntity findByPosition(Long position) {

        if (position == null) {
            throw new VariantException(ERROR_VARIANT_POSITION_REQUIRED);
        }

        Variant variant = variantRepository.findByPosition(position);
        VariantDTO variantDTO = dataMapper.convert(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }

    public ResponseEntity findByAlteration(String alteration) {

        if (alteration == null) {
            throw new VariantException(ERROR_VARIANT_ALTERATION_REQUIRED);
        }

        Variant variant = variantRepository.findByAlteration(alteration);
        VariantDTO variantDTO = dataMapper.convert(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }

    public ResponseEntity findByChromosome(String chromosome) {

        if (chromosome == null) {
            throw new VariantException(ERROR_VARIANT_CHROMOSOME_REQUIRED);
        }

        Variant variant = variantRepository.findByChromosome(chromosome);
        VariantDTO variantDTO = dataMapper.convert(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }

    public ResponseEntity findAll() {

        if (variantRepository.findAll().isEmpty()) {
            throw new VariantException(ERROR_THERE_IS_NO_VARIANT_YET);
        }

        List<Variant> variants = variantRepository.findAll();
        List<VariantDTO> variantDTO = new LinkedList<>();
        VariantDTOs variantDTOs = new VariantDTOs();

        variants.forEach(variant -> {
            VariantDTO item = dataMapper.convert(variant);
            variantDTO.add(item);
        });
        variantDTOs.setVariantDTOList(variantDTO);

        return new ResponseEntity<>(variantDTOs, HttpStatus.OK);
    }

    public ResponseEntity createVariant(VariantDTO variantDTO) {

        if (StringUtils.isEmpty(variantDTO.getAlteration())) {
            throw new VariantException(ERROR_VARIANT_ALTERATION_REQUIRED);
        }

        Variant variant = new Variant();
        variant.setPosition(variantDTO.getPosition());
        variant.setAlteration(variantDTO.getAlteration());
        variant.setChromosome(variantDTO.getChromosome());
        variant.setUser(variantDTO.getUser());
        variantRepository.saveAndFlush(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }


    public ResponseEntity updateVariant(VariantDTO variantDTO) {

        if (StringUtils.isEmpty(variantDTO.getAlteration())) {
            throw new VariantException(ERROR_VARIANT_ALTERATION_REQUIRED);
        }

        Variant variant = variantRepository.findByPosition(variantDTO.getPosition());
        if (variant != null) {
            variant.setPosition(variantDTO.getPosition());
            variant.setAlteration(variantDTO.getAlteration());
            variant.setChromosome(variantDTO.getChromosome());
            variant.setDescription(variantDTO.getDescription());
            variant.setUser(variantDTO.getUser());
        }
        variantRepository.saveAndFlush(variant);
        variantDTO = dataMapper.convert(variant);

        return new ResponseEntity<>(variantDTO, HttpStatus.OK);
    }

    public ResponseEntity deleteVariantById(Long id) {

        if (id == null) {
            throw new VariantException(ERROR_VARIANT_ID_REQUIRED);
        }
        Variant variant = variantRepository.findOne(id);
        variantRepository.deleteById(id);
        return new ResponseEntity<>(variant, HttpStatus.GONE);
    }

    public ResponseEntity deleteAll() {

        List<Variant> variants = variantRepository.findAll();
        variantRepository.deleteAll();
        return new ResponseEntity<>(variants, HttpStatus.GONE);
    }
}
