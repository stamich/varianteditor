package me.imagene.varianteditor.controller;

import me.imagene.varianteditor.dto.VariantDTO;
import me.imagene.varianteditor.model.Role;
import me.imagene.varianteditor.model.Variant;
import me.imagene.varianteditor.service.VariantServiceImpl;
import me.imagene.varianteditor.utility.DataMapper;
import me.imagene.varianteditor.utility.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/variant")
@RolesAllowed({Role.ADMIN, Role.DBA, Role.USER})
public class VariantController {

    private DataMapper<VariantDTO, Variant> dataMapper;
    private VariantServiceImpl variantService;

    @Autowired
    public VariantController(DataMapper<VariantDTO, Variant> dataMapper, VariantServiceImpl variantService) {
        this.dataMapper = dataMapper;
        this.variantService = variantService;
    }

    @RequestMapping(value = "/find{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return variantService.findOne(id);
    }

    @RequestMapping(value = "/find_position{position}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findByPosition(@PathVariable Long position) {
        return variantService.findByPosition(position);
    }

    @RequestMapping(value = "/find_alteration{alteration}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findByAlteration(@PathVariable String alteration) {
        return variantService.findByAlteration(alteration);
    }

    @RequestMapping(value = "/find_chromosome{chromosome}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findByChromosome(@PathVariable String chromosome) {
        return variantService.findByChromosome(chromosome);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findAllVariants() {
        return variantService.findAll();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(VariantDTO variantDTO) {
        return variantService.createVariant(variantDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> update(VariantDTO variantDTO) {
        return variantService.updateVariant(variantDTO);
    }

    @RequestMapping(value = "/delete{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        return variantService.deleteVariantById(id);
    }

    @RequestMapping(value = "/delete_all", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> deleteAll() {
        return variantService.deleteAll();
    }
}
