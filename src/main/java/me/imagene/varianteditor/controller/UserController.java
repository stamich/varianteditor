package me.imagene.varianteditor.controller;

import me.imagene.varianteditor.dto.UserDTO;
import me.imagene.varianteditor.model.Role;
import me.imagene.varianteditor.model.User;
import me.imagene.varianteditor.service.UserServiceImpl;
import me.imagene.varianteditor.utility.DataMapper;
import me.imagene.varianteditor.utility.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/user")
@RolesAllowed(Role.ADMIN)
public class UserController {

    public DataMapper<UserDTO, User> dataMapper;
    public UserServiceImpl userService;

    @Autowired
    public UserController(DataMapper<UserDTO, User> dataMapper, UserServiceImpl userService) {
        this.dataMapper = dataMapper;
        this.userService = userService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO) {
        //User user = dataMapper.convert(userDTO);
        return userService.createUser(userDTO);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> login(@RequestBody UserDTO userDTO) {
        //User user = dataMapper.convert(userDTO);
        return userService.login(userDTO);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> listAllUsers() {
        return userService.findAll();
    }

    @RequestMapping(value = "/find{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> findUserById(@PathVariable Long id) {
        return userService.findOne(id);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO) {
        return userService.updateUser(userDTO);
    }
}
