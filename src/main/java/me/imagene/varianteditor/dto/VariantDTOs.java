package me.imagene.varianteditor.dto;

import lombok.Data;

import java.util.List;

@Data
public class VariantDTOs {

    private List<VariantDTO> variantDTOList;
}
