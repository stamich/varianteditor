package me.imagene.varianteditor.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTOs {

    private List<UserDTO> userDTOList;
}
