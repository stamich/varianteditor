package me.imagene.varianteditor.dto;

import lombok.Data;
import lombok.ToString;
import me.imagene.varianteditor.model.User;

@Data
@ToString
public class VariantDTO {

    private Long position;
    private String alteration;
    private String chromosome;
    private String description;
    private User user;
}
