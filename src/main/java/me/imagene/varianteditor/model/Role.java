package me.imagene.varianteditor.model;

public enum Role {
    ADMIN,
    DBA,
    READER,
    USER;
}
