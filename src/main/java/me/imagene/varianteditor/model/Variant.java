package me.imagene.varianteditor.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "variant")
@Data
@EqualsAndHashCode(callSuper = true)
public class Variant extends AbstractModel<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private Long position;

    @Column(nullable = false, unique = true)
    private String alteration;

    @Column(nullable = false, unique = true)
    private String chromosome;

    @Column(length = 5000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;
}
