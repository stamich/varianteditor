package me.imagene.varianteditor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VariantEditorApplication {

    public static void main(String[] args) {
        SpringApplication.run(VariantEditorApplication.class, args);
    }

}
