package me.imagene.varianteditor.utility;

import me.imagene.varianteditor.dto.VariantDTO;
import me.imagene.varianteditor.model.Variant;
import org.springframework.stereotype.Component;

@Component
public class VariantDtoMapper implements DataMapper<VariantDTO, Variant> {

    @Override
    public Variant convert(VariantDTO variantDTO) {

        Variant variant = new Variant();
        variant.setPosition(variantDTO.getPosition());
        variant.setAlteration(variantDTO.getAlteration());
        variant.setChromosome(variantDTO.getChromosome());
        variant.setDescription(variantDTO.getDescription());
        variant.setUser(variantDTO.getUser());

        return variant;
    }
}
