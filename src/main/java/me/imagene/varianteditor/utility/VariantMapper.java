package me.imagene.varianteditor.utility;

import me.imagene.varianteditor.dto.VariantDTO;
import me.imagene.varianteditor.model.Variant;
import org.springframework.stereotype.Component;

@Component
public class VariantMapper implements DataMapper<Variant, VariantDTO> {

    @Override
    public VariantDTO convert(Variant variant) {

        VariantDTO variantDTO = new VariantDTO();

        variantDTO.setPosition(variant.getPosition());
        variantDTO.setAlteration(variant.getAlteration());
        variantDTO.setChromosome(variant.getChromosome());
        variantDTO.setDescription(variant.getDescription());
        variantDTO.setUser(variant.getUser());

        return variantDTO;
    }
}
