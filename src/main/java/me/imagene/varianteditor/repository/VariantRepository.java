package me.imagene.varianteditor.repository;

import me.imagene.varianteditor.model.User;
import me.imagene.varianteditor.model.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariantRepository extends JpaRepository<Variant, Long> {

    @Query("SELECT v FROM Variant v WHERE v.id = :id")
    Variant findOne(@Param("id") Long id);

    @Query("SELECT v FROM Variant v")
    List<Variant> findAll();

    @Query("SELECT v FROM Variant v WHERE v.position = :position")
    Variant findByPosition(Long position);

    @Query("SELECT v FROM Variant v WHERE v.alteration = :alteration")
    Variant findByAlteration(String alteration);

    @Query("SELECT v FROM Variant v WHERE v.chromosome = :chromosome")
    Variant findByChromosome(String chromosome);

    void deleteById(Long id);
    void deleteAll();
}
