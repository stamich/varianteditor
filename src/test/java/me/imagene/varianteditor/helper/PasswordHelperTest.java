package me.imagene.varianteditor.helper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PasswordHelperTest {

    @Autowired
    private PasswordHelper passwordHelper;

    private String password = "password";

    @Test
    public void hashPasswordTest() {
        assertNotNull(passwordHelper.hashPassword(password));
        assertNotEquals(password, passwordHelper.hashPassword(password));
    }

    @Test
    public void isPasswordValidTest() {
        String hashedPassword = passwordHelper.hashPassword(password);
        assertTrue(passwordHelper.isPasswordValid(password, hashedPassword));
    }

    @Test
    public void isPasswordStrongTest() {
        assertFalse(PasswordHelper.isPasswordStrong(password));
        assertFalse(PasswordHelper.isPasswordStrong("abcd 1btst"));
        assertTrue(PasswordHelper.isPasswordStrong("abcdefg1"));
    }
}
