package me.imagene.varianteditor.helper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TokenHelperTest {

    @Autowired
    private TokenHelper tokenHelper;

    @Test
    public void generateTokenTest() {
        Long userId = 102L;
        assertNotNull(tokenHelper.generateToken(userId));
        assertTrue(tokenHelper.generateToken(userId).contains("-102"));
    }
}
