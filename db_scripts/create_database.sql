DROP DATABASE variant_db;
CREATE DATABASE variant_db
    WITH
    OWNER = michal
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

-- CREATE DATABASE variant_db
--     WITH
--     OWNER = michal
--     ENCODING = 'UTF8'
--     LC_COLLATE = 'Polish_Poland.1250'
--     LC_CTYPE = 'Polish_Poland.1250'
--     TABLESPACE = pg_default
--     CONNECTION LIMIT = -1;